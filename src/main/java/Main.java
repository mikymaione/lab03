/*
MIT License
Copyright (c) 2018 Michele Maione
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import java.util.ArrayList;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private final static Random r = new Random();
    private final static Logger logger = LoggerFactory.getLogger(Main.class);


    public static void main(String[] args) {
        NuovaPartita();
    }

    private static void NuovaPartita() {
        ArrayList<Integer> tiri = new ArrayList<>();
        Game g = new Game();

        for (int i = 0; i < 10; i++) {
            int rndI = r.nextInt(10) + 1;

            g.roll(rndI);

            tiri.add(rndI);
        }

        int x = 0;
        String t = "Partita bowling\n";
        for (Integer punto : tiri)
            t += (x += 1) + "# " + punto + "\n";
        t += "\nScore: " + g.score() + "\n";

        logger.info(t);
    }
}