# Bowling Game Kata (di [UncleBob](http://butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata))

## Report
https://mikymaione.gitlab.io/lab03/

Il gioco del *bowling* divide la gara di ciascun giocatore in 10 **frame**: in
ogni *frame* il giocatore ha 2 possibilità di abbattere i 10 birilli (*pin*). Il
punteggio ottenuto nel *frame* è il numero di birilli abbattuti, maggiorato di
un premio per gli **spare** e gli **strike**.

Uno *spare* si verifica quando vengono abbattuti 10 birilli usando i 2
tentativi. In questo caso il premio è il numero di birilli abbattuto con il tiro
(*roll*) seguente (effettuato nel prossimo *frame*).

Uno *strike* si verifica quando vengono abbattuti 10 birilli al primo tentativo:
in questo caso il secondo tiro del *frame* non viene effettuato. Il premio per
lo *strike* è il numero di birilli abbattuto con i 2 tiri seguenti (effettuati
nel prossimo *frame*).

Se uno *strike* o uno *spare* si verificano nel decimo *frame*, il giocatore ha
diritto ai tiri necessari ad acquisire il premio relativo. Il decimo *frame* può
quindi dare luogo a un massimo di 3 tiri. Il punteggio massimo ottenibile è 300
punti.

```java
class Game {
 public void roll(int pins){}
 public int score(){}
}
```

# Esercizio 1 

(tempo stimato 45')

Il progetto viene già fornito con i test di unità di Uncle Bob
(`BowlingGameTest.java`).

## *Setup* di Gradle

Allestire correttamente l'ambiente di progetto con Gradle, in modo che sia
possibile compilare con Java ed effettuare i test con la libreria `junit`. Alla
fine dovrete ottenere:

- le *directory* previste dalle convenzioni Gradle per i progetti Java, che
  userete per i *file* `.java` che compongono il progetto
- un opportuno *file* `build.gradle` che utilizza i *plugin* adatti al progetto
  e permette di ottenere il fallimento dei test forniti con il comando `gradle
  test`

Suggerimento:
[*Quickstart* Gradle per Java](https://docs.gradle.org/current/userguide/tutorial_java_projects.html)

## *Setup* del *repository*

Aggiungere il *wrapper* di Gradle al *repository* git.

Vedi [Documentazione *wrapper*](https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:wrapper_generation)

## Setup del *repository* GitLab

Configurare la *continuous integration* (CI) su GitLab
[(documentazione)](https://docs.gitlab.com/ce/ci/README.html).

# Esercizio 2

(tempo stimato 2h15m)

## Superare i test

Implementare la classe `Game` in modo da superare i test.

## Un programma *client* per `Game`

Scrivere un programma (con un `main`) che utilizzi la classe `Game` per simulare
una partita. Il programma deve stampare l'evoluzione del punteggio con la
libreria [`SLF4J`](http://www.slf4j.org/).

- utilizzare il *plugin*
  [`application`](https://docs.gradle.org/current/userguide/application_plugin.html).
- creare un rapporto di copertura con il *plugin*
  [`jacoco`](https://docs.gradle.org/current/userguide/jacoco_plugin.html).
- aggiungere la possibilità di scaricare i rapporti generati da `jacoco` (Vedi:
  https://docs.gitlab.com/ce/ci/yaml/README.html#artifacts).
- Il *coverage* può essere visualizzato impostando l'espressione regolare
  `Total.{1,50}>(\d{1,3})%` nel `Test coverage parsing` dei `Pipelines settings`
  e aggiungendo un comando che stampi su *standard output* l'`index.html` creato
  da `jacoco`.

  
